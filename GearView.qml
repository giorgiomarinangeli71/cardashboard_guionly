import QtQuick 2.10

Rectangle {

    id: prindle

    width: 80
    height: 30

    color: "transparent"

    Row {

        spacing: 10

        Image {
            id: img_p
            source: "assets/images/Pr.png"

        }
        Image {
            id: img_r
            source: "assets/images/Rr.png"

        }
        Image {
            id: img_n
            source: "assets/images/Nr.png"

        }
        Image {
            id: img_d
            source: "assets/images/Dr.png"

        }
    }

}
